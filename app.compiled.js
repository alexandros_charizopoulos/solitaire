'use strict';

var _angular = require('angular');

var _angular2 = _interopRequireDefault(_angular);

require('ngDraggable');

require('klondike/klondike.js');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_angular2.default.module('solitaire', ['klondike', 'ngDraggable']);

_angular2.default.element(document).ready(function () {
	_angular2.default.bootstrap(document, ['solitaire']);
});
