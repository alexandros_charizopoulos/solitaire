var normalize = System.normalize
var systemLocate = System.locate;
var systemFetch = System.fetch;
var systemTranslate = System.translate;
var systemInstantiate = System.instantiate;

System.normalize = function (name, parentName, parentAddress) {
  console.log('normalize:' + JSON.stringify({
      name: name,
      parentName: parentName,
      parentAddress: parentAddress
    }));
  return normalize.call(this, name, parentName, parentAddress)
};

System.locate = function (load) {
	console.log('locating:' + JSON.stringify({
		load: load
	}));
	return systemLocate.call(this, load);
};

System.fetch = function (load) {
	console.log('fetching:' + JSON.stringify({
		load: load
	}));
	return systemFetch.call(this, load);
};

System.translate = function (load) {
	console.log('translate:' + JSON.stringify({
		load: load
	}));
	return systemTranslate.call(this, load);
};

System.instantiate = function (load) {
	console.log('instantiate:' + JSON.stringify({
		load: load
	}));
	return systemInstantiate.call(this, load);
};